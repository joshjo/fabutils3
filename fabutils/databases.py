import os
import datetime

from fabric.api import local, prompt, env, get
from fabric.colors import green, red
from .utils import VirtualenvMixin


class DatabaseRestore(object):
    def __init__(self, db_env, credentials, dump_file):
        self.dump_file = dump_file
        self.credentials = credentials
        print(green("Installing snapshot %s" % dump_file))

    def fab_drop_database(self):
        raise NotImplemented("Please implement this method")

    def fab_load_datafile(self):
        raise NotImplemented("Please implement this method")

    def fab_restore_database(self):
        if not prompt(red("I'm gonna destroy your local database."
                          " Are you sure? y/n")).lower() == 'y':
            return

        if env.is_production:
            print(red("Sorry this command cannot be run in production"))

        self.fab_drop_database()
        self.fab_load_datafile()


class MysqlOptionsMixin(object):
    def parse_options(self):
        options = {
            '-h ': self.credentials['HOST'],
            '-u ': self.credentials['USER'],
            '-p': self.credentials['PASSWORD'],
        }
        if not self.credentials['PASSWORD']:
            del options['-p']

        if not self.credentials['HOST']:
            del options['-h ']
        return ' '.join(["%s%s" % x for x in options.items()])


class PostgresqlOptionsMixin(object):
    def parse_options(self):
        options = {
            '-h ': self.credentials['HOST'],
            '-U ': self.credentials['USER'],
        }

        if not self.credentials['HOST']:
            del options['-h ']
        return ' '.join(["%s%s" % x for x in options.items()])


class MysqlDatabaseRestore(MysqlOptionsMixin, DatabaseRestore):
    DB_COMMAND = 'mysql'

    def fab_drop_database(self):
        """
        Drops a database
        """
        local("echo 'drop database {NAME}; "
              "create database {NAME} "
              "CHARACTER SET utf8 COLLATE utf8_general_ci;'"
              " | {command} {options}".format(
                  command=self.DB_COMMAND,
                  options=self.parse_options(),
                  **self.credentials))

    def fab_load_datafile(self):
        """
        Loads a database backup
        """
        local("{command} {options} {NAME} < {file}".format(
            command=self.DB_COMMAND,
            options=self.parse_options(),
            file=self.dump_file,
            **self.credentials
        ))


class PostgresqlDatabaseRestore(PostgresqlOptionsMixin, DatabaseRestore):
    DB_COMMAND = 'psql'

    def fab_drop_database(self):
        """
        Drops a database
        """
        local('psql -U {USER} postgres -c "drop database {NAME};"'.format(
            **self.credentials))
        local(
            'psql -U {USER} postgres -c "create database {NAME}'
            ' owner {USER};"'.format(**self.credentials))

    def fab_load_datafile(self):
        """
        Loads a database dump
        """
        local("{command} -q -U {USER} {NAME} < {file}".format(
            command=self.DB_COMMAND,
            file=self.dump_file,
            **self.credentials))


class DatabaseBackup(object):
    def __init__(self, db_env, credentials, backup_dir, run_func):
        self.credentials = credentials
        self.db_env = db_env
        self.backup_dir = backup_dir
        self.run_func = run_func

    def generate_destination_file(self):
        return '%s%s-%s.sql' % (
            self.backup_dir,
            self.db_env,
            datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))

    def parse_options(self):
        raise NotImplemented("Please implement this method")

    def fab_create_backup(self):
        """
        Creates a database backup
        """
        dest_file = "%s.gz" % self.generate_destination_file()
        print(green(
            "Creating backup for databse %s" % self.credentials['NAME']))
        command = (
            "{command} {options} {db_name} | gzip > {dest_file}").format(
            command=self.DB_COMMAND,
            options=self.parse_options(),
            db_name=self.credentials['NAME'],
            dest_file=dest_file)
        self.run_func(command)
        return dest_file


class PostgresqlDatabaseBackup(PostgresqlOptionsMixin, DatabaseBackup):
    DB_COMMAND = 'pg_dump'


class MySqlDatabaseBackup(MysqlOptionsMixin, DatabaseBackup):
    DB_COMMAND = 'mysqldump'


class RemoteDatabaseOperations(VirtualenvMixin):
    def fab_make_remote_backup(self):
        """
        Creates a database backup in a remote server
        """
        print(green("Creating remote database backups"))
        self.fab_run_env(
            "fab backup_local_databases -f %sfabfile.py" % env.core_dir)


class LocalDatabaseOperations(VirtualenvMixin, object):
    db_restore_handler_class = None
    db_backup_handler_class = None

    def fab_refresh_db(self):
        """
        Destroys the localdatabase and recreates it with fresh data from
        the selected server
        """
        print(green("Packing remote database backups"))
        timestamp = datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
        self.fab_run_env(
            "fab pack_local_databases:folder={folder}"
            " -f {core_dir}fabfile.py".format(
                core_dir=env.core_dir, folder=timestamp))
        print(green("Downloading packed databases"))
        files = get("db-bundle.tar.gz")
        for f in files:
            try:
                # this is only for linux
                out = local("tar zxvf %s --warning=no-timestamp 2>&1" % f,
                            capture=True)
            except:
                out = local("tar zxvf %s 2>&1" % f, capture=True)
            out = out.replace("x ", "")
            sql_files = out.split("\n")[1:]

            for sql_file in sql_files:
                print(green("Refreshing from file %s" % sql_file))
                filename = sql_file
                db_env = [x for x in self.settings.DATABASES.keys()
                          if x in sql_file][0]
                local("gunzip %s" % sql_file)
                filename = sql_file.replace(".gz", "")
                restore = self.db_restore_handler_class(
                    db_env, self.settings.DATABASES[db_env], filename)
                restore.fab_restore_database()
                local("rm %s" % filename)

    def fab_pack_local_databases(self, folder='default'):
        """
        Creates a backup of all local databases and stores them in a .tar.gz
        file stored in the /tmp/ directory
        """
        print (green("Creating backups for %d databases" %
                     len(self.settings.DATABASES.keys())))
        # timestamp = datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
        path = os.path.join("/tmp", "db-pack-%s" % folder)
        local("mkdir %s" % path)

        for db_env, database in self.settings.DATABASES.items():
            db_backup = self.db_backup_handler_class(
                db_env, database, "%s/" % path, local)
            db_backup.fab_create_backup()

        local("tar zcvf db-bundle.tar.gz {path}".format(path=path))

    def fab_backup_local_databases(self):
        """
        Creates a database backup and stores it in the directory BACKUP_DIR
        of your settings.py
        """
        print (green("Creating backups for %d databases" %
                     len(self.settings.DATABASES.keys())))
        for db_env, database in self.settings.DATABASES.items():
            db_backup = self.db_backup_handler_class(
                db_env, database, self.settings.DB_BACKUP_DIR, local)
            db_backup.fab_create_backup()
