import os

from fabric.colors import green, red
from fabric.api import get, sudo

from .utils import VirtualenvMixin


class UpgradePackages(VirtualenvMixin):

    @staticmethod
    def fab_list_packages():

        """
        In order to prevent malfunctions when updating packages on the server
        and avoid to make them one by one, this command allows us to update a
        list of packages.
        """

        print(green('\n*** Package listing started ***\n'))
        sudo(
            'apt-get dist-upgrade --dry-run | grep ^Inst | cut -d" " -f2 > '
            '/tmp/packages_to_update.txt')
        get('/tmp/packages_to_update.txt')

    def fab_upgrade_listed_packages(self):

        """
        In order to prevent malfunctions when updating packages on the server
        and avoid to make them one by one, this command upgrades a set of
        packages provided in the file
        /path/to/project/{host}/packages_to_update.txt on the
        selected server. Use it together with the list_packages command.
        """

        print(green('\n*** Update listed packages started ***\n'))
        if os.path.isfile("./{host}/packages_to_update.txt".format(
                host=self.env.host_string)):
            with open("{host}/packages_to_update.txt".format(
                    host=self.env.host_string), "r") as list_of_packages:
                for package in list_of_packages:
                    if not package.strip() == '':
                        sudo('apt-get install --only-upgrade %s' % (
                            package.rstrip().partition(' ')[0]))
        else:
            print(red(
                'Error: List of packages to update not found on current path,'
                '\n       please check if "packages_to_update.txt" file exist '
                'on the same level as "fabfile.py".'))
