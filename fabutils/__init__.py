from .server import *
from .databases import *


import inspect
import sys


def _register_fabric_class_task(instance, module_name, settings):

    module_obj = sys.modules[module_name]
    method_names_list = []
    setattr(instance, 'settings', settings)
    # Iterate over the methods of the class and dynamically create a function
    # for each method that calls the method and add it to the current module
    for method in inspect.getmembers(instance, predicate=inspect.ismethod):
        method_name = method[0]

        if method_name.startswith('fab_'):
            # get the bound method
            func = getattr(instance, method_name)

            method_name = method_name.replace('fab_', '')

            # add the function to the current module
            setattr(module_obj, method_name, func)
            method_names_list += [method_name]

    return method_names_list


def register_class(cls, settings):
    instance = cls()
    _register_fabric_class_task(instance, 'fabfile', settings)


def func(ev, content):
    def inner():
        """
        Connects to the selected server
        """
        [setattr(env, k, v) for k, v in content.items()]
    return inner


def autodiscover_environments(settings):
    module_obj = sys.modules['fabfile']
    for ev, content in settings.ENVIRONMENTS.items():
        setattr(module_obj, ev, func(ev, content))
