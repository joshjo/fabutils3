from fabric.api import prefix, run, env


class VirtualenvMixin(object):

    def fab_run_env(self, cmd):
        """
        Runs commands using virtualenv
        """
        with prefix('source %s/../bin/activate' % env.core_dir):
            run(cmd)
