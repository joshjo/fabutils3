from fabric.colors import green
from fabric.api import run, get

from .utils import VirtualenvMixin


class Translations(VirtualenvMixin):
    languages = []

    def fab_makemessages(self):

        """
        Creates .po translation files
        """

        print(green("Creating .po files"))
        run('mkdir -p locale')
        for lang in self.languages:
            self.fab_run_env('python manage.py makemessages -l %s' % lang)

    def fab_compile_messages(self):

        """
        Compiles translation files
        """

        print(green("Making translations"))
        self.fab_run_env("django-admin compilemessages")

    def fab_download_locale(self):

        """
        Downloads locale data for the local translations
        """

        print(green("Packing locale translations"))
        run("tar -zcvf /tmp/locale.tar.gz {0}/locale".format(
            self.env.core_dir))
        print(green("You're gonna download: "))
        run("du -hs /tmp/locale.tar.gz")
        files = get("/tmp/locale.tar.gz")
        for f in files:
            print(green("Download file %s, now you can copy it" % f))
        run("rm -r /tmp/locale.tar.gz")
